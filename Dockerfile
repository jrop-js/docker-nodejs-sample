FROM node:alpine
ADD ./index.js /
ADD ./package.json /
CMD ["node", "/index.js"]
