const http = require('http')
const os = require('os')

const TPL = () => `
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sample NodeJS App</title>
</head>
<body>
	<h1>Index</h1>
	<ul>
		<li>hostname: ${os.hostname()}</li>
		<li>platform: ${os.platform()} ${os.release()} ${os.arch()}</li>
		<li>username: ${os.userInfo().username}</li>
		<li>uptime: ${os.uptime()}</li>
		<li>version: ${require('./package').version}</li>
	</ul>
</body>
</html>
`

require('http')
	.createServer((req, res) => res.end(TPL()))
	.listen(process.env.PORT || 80)